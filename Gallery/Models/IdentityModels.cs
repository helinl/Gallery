﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Gallery.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public ICollection<Picture> Pictures { get; set; }
        public ICollection<Folder> Folders { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Vote> Votes { get; set; }
        public ICollection<FolderLike> FolderLikes { get; set; }
        public string NickName { get; set; }
    }

    public class GalleryContext : IdentityDbContext<ApplicationUser>
    {
        public GalleryContext()
            : base("GalleryContext", throwIfV1Schema: false)
        {
        }

        public static GalleryContext Create()
        {
            return new GalleryContext();
        }

        public System.Data.Entity.DbSet<Gallery.Models.Picture> Pictures { get; set; }
        public System.Data.Entity.DbSet<Gallery.Models.Folder> Folders { get; set; }
        public System.Data.Entity.DbSet<Gallery.Models.Vote> Votes { get; set; }
        public System.Data.Entity.DbSet<Gallery.Models.Comment> Comments { get; set; }

        public System.Data.Entity.DbSet<Gallery.Models.FolderLike> FolderLikes { get; set; }
    }



}