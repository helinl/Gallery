﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gallery.Models;
using System.IO;
using Gallery.CustomFilters;
using Microsoft.AspNet.Identity;

namespace Gallery.Controllers
{
    public class PictureController : Controller
    {
        private GalleryContext db = new GalleryContext();

        public ActionResult Index(int? id)
        {
            if(id == null)
                return RedirectToAction("Index", "Folder");
            ViewBag.FolderId = id;
            Picture picture = db.Pictures.Find(id);
            Folder folder = db.Folders.Find(id);
            ViewBag.FolderTitle = folder.Title;

            var pictures = db.Pictures.Include(x => x.Votes).ToList();
            pictures = db.Pictures.Include(x => x.Comments).ToList();
            pictures = db.Pictures.Include(x => x.User).ToList();


            return View(db.Pictures.Where(x => x.FolderId == id).ToList());
        }

        // GET: Picture/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);

            
            ApplicationUser user = db.Users.Find(picture.UserId);
            ViewBag.UserName = user.NickName;

            var comments = db.Comments.Include("User").Where(x => x.PictureId == id).ToList();

            ViewBag.Comments = comments;
 
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);


        }

        // GET: Picture/Create
        [Authorize]
        public ActionResult Create(int id)
        {
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", id);
            return View();
        }

        // POST: Picture/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Title,Description,DateAdded,AddedPicture,FolderId,AddedPicture")] Picture picture, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                picture.DateAdded = DateTime.Now;
                if (file != null)
                {
                    if (file.ContentType != "image/jpeg" && file.ContentType != "image/png")
                    {
                        return Content("Lisada saab ainult pildifaile");
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                        picture.AddedPicture = array;
                        db.Pictures.Add(picture);
                    }
                }

                picture.UserId = User.Identity.GetUserId();
                db.SaveChanges();

                return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
            return View(picture);
        }

        [Authorize]
        public ActionResult FileUpload(HttpPostedFileBase file, int Id)
        {
            Picture picture = db.Pictures.Find(Id);
            if (picture == null)
            {
                return Content("Sellist pilti pole");
            }
            if (file != null)
            {
                if(file.ContentType != "image/jpeg" && file.ContentType != "image/png")
                {
                    return Content("Lisada saab ainult pildifaile");
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    picture.AddedPicture = array;
                    db.Entry(picture).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return Content("Unustasid faili valida");
        }

        public ActionResult Show(int id)
        {
            var picture = db.Pictures.Find(id);
            if (picture.AddedPicture == null)
            {
                return Content("Pilti pole");
            }
            return File(picture.AddedPicture, "image/jpg");
        }

        public ActionResult ShowId(string title)
        {
            var picture = db.Pictures.Find(title);
            if (picture.AddedPicture == null)
            {
                return Content("Pilti pole");
            }
            return File(picture.AddedPicture, "image/jpg");
        }


        // GET: Picture/Edit/5
        
        public ActionResult Edit(int? id)
        {
            Picture picture = db.Pictures.Find(id);

            if(User.IsInRole("Admin") || picture.UserId == User.Identity.GetUserId())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                if (picture == null)
                {
                    return HttpNotFound();
                }
                ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
                return View(picture);
            }
            else
            {
                return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
            }
           
            
        }

        // POST: Picture/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,DateAdded,FolderId,AddedPicture")] Picture picture)
        {
            Picture oldPicture = db.Pictures.Find(picture.Id);
            if (User.IsInRole("Admin") || oldPicture.UserId == User.Identity.GetUserId())
            {
                if (ModelState.IsValid)
                {
                    oldPicture.Title = picture.Title;
                    oldPicture.Description = picture.Description;
                    oldPicture.FolderId = picture.FolderId;
                    db.Entry(oldPicture).State = EntityState.Modified;
                    db.SaveChanges();


                    return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
                }
                ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
                return View(picture);
            }
            else
            {
                return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
            }
            
        }

        // GET: Picture/Delete/5
        public ActionResult Delete(int? id)
        {
            Picture picture = db.Pictures.Find(id);
            if (User.IsInRole("Admin") || picture.UserId == User.Identity.GetUserId())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                
                if (picture == null)
                {
                    return HttpNotFound();
                }
                
                return View(picture);
            }
            else
            {
                return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
            }

            
        }

        // POST: Picture/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);
            db.Pictures.Remove(picture);
            List <Comment> comments = db.Comments.Where(x => x.PictureId == id).ToList();
            db.Comments.RemoveRange(comments);
            List<Vote> votes = db.Votes.Where(x => x.PictureId == id).ToList();
            db.Votes.RemoveRange(votes);
            db.SaveChanges();
            return RedirectToAction("Index", "Folder");
            //return RedirectToAction("Index", "Picture", new { Id = db.Pictures.Find(picture.Id).FolderId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
