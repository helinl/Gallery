﻿using Gallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.IO;
using System.Data.SqlClient;

namespace Gallery.Controllers
{
    public class HomeController : Controller
    {
        private GalleryContext db = new GalleryContext();

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //GET
        public ActionResult CreateUser()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser([Bind(Include = "Id, UserName, Email, Password")] User user)
        {
            if (ModelState.IsValid)
            {
                var enteredUser = db.Users.Where(x => x.UserName == user.UserName).FirstOrDefault();
                if (enteredUser == null)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "Sellise nimega kasutaja on juba olemas");
            }
            return View(user);
        }

        //GET
        public ActionResult LogIn()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LogIn(string userName, string password)
        {
            var user = db.Users.Where(x => x.UserName == userName && x.Password == password).FirstOrDefault();
            if (user == null)
            {
                ViewBag.Error = "Sisestati vale kasjutajanimi või parool";
                return RedirectToAction("Login");
            }
            return RedirectToAction("Index");
        }
        // kuidas jääks sisselogituks kogu külastuse jooksul??

        public ActionResult FileUpload(HttpPostedFileBase file, int PictureID)
        {
            if (file != null)
            {

                // save the image path path to the database or you can send image
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }

            }
            // after successfully uploading redirect the user
            return RedirectToAction("actionname", "controller name");
        }

        public ActionResult Index(string sortOrder)
        {
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            var pictures = db.Pictures.OrderByDescending(s => s.DateAdded).Take(3).ToList();

            //KATSETUSED:
            //var pictureId = db.Votes.Count(x => x.PictureId.Value == ).ToList();
            //int count = (from x in db.Votes where x == ... select x).Count();

            //ViewBag.DateSortParm = sortOrder == "Vote" ? "vote_Asc" : "Vote";

            //var pictures = db.Votes.GroupBy(x => x.PictureId).Select(y => new { pictureId = y.Key, Count = y.Distinct().Count() });
            //pictures = db.Votes.OrderByDescending(s => s.Votes).Take(3).ToList();

            return View(pictures);

        }


    }
}