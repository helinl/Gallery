﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Gallery.Models;
using System.IO;

namespace Gallery.Controllers
{
    public class PictureController : Controller
    {
        private GalleryContext db = new GalleryContext();

        // GET: Picture

        // GET: Picture/Create
        public ActionResult Comment()
        {
            return View("Index");
        }
         
        // POST: Comment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Comment([Bind(Include = "Id, PictureId, FolderId, UserId, DateAdded, CommentAdded")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Index");
        }

        // GET: Folder/Vote
        public ActionResult Vote()
        {
            return View("Index");
        }

        // POST: Folder/Vote
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vote([Bind(Include = "Id, UserId, FolderId, PictureId")] Vote vote)
        {
            if (ModelState.IsValid)
            {
                db.Votes.Add(vote);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Index");
        }


        public ActionResult Index()
        {
            //Folder title = new Folder();

            //ViewBag.FolderTitle = title;

            
            return View(db.Pictures.ToList());
        }

        // GET: Picture/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // GET: Picture/Create
        public ActionResult Create()
        {
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title");
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Picture/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,DateAdded,AddedPicture,UserId,FolderId,AddedPicture")] Picture picture, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                picture.DateAdded = DateTime.Now;
                if (file != null)
                {
                    if (file.ContentType != "image/jpeg" && file.ContentType != "image/png")
                    {
                        return Content("Lisada saab ainult pildifaile");
                    }
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                        picture.AddedPicture = array;
                        db.Pictures.Add(picture);
                    }
                }
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", picture.UserId);
            return View(picture);
        }

        public ActionResult FileUpload(HttpPostedFileBase file, int Id)
        {
            Picture picture = db.Pictures.Find(Id);
            if (picture == null)
            {
                return Content("Sellist pilti pole");
            }
            if (file != null)
            {
                if(file.ContentType != "image/jpeg" && file.ContentType != "image/png")
                {
                    return Content("Lisada saab ainult pildifaile");
                }
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                    picture.AddedPicture = array;
                    db.Entry(picture).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return Content("Unustasid faili valida");
        }

        public ActionResult Show(int id)
        {
            var picture = db.Pictures.Find(id);
            if (picture.AddedPicture == null)
            {
                return Content("Pilti pole");
            }
            return File(picture.AddedPicture, "image/jpg");
        }


        // GET: Picture/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", picture.UserId);
            return View(picture);
        }

        // POST: Picture/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,DateAdded,UserId,FolderId,AddedPicture")] Picture picture)
        {
            if (ModelState.IsValid)
            {
                Picture oldPicture = db.Pictures.Find(picture.Id);
                oldPicture.Title = picture.Title;
                oldPicture.Description = picture.Description;
                oldPicture.FolderId = picture.FolderId;
                db.Entry(oldPicture).State = EntityState.Modified;
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            ViewBag.FolderId = new SelectList(db.Folders, "Id", "Title", picture.FolderId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "UserName", picture.UserId);
            return View(picture);
        }

        // GET: Picture/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // POST: Picture/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);
            db.Pictures.Remove(picture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
